# 1.3 QuerySet
## Сохранение объектов в БД

---
##### 1. Сохраните несколько объектов модели "Экстренных служб", "Заявителя" и "Обращения" двумя способами (методом create уровня менеджера запросов objects и методом save уровня экземпляра модели)

>     models.Service.objects.create(name='Пожарная', 
>                                   code='01', 
>                                   number=83476535695)
> 
> 
>     models.Service(name='Полиция', 
>                    code='02', 
>                    number=83569545323).save()

---
##### 2. Создайте "Обращение" через менеджер запросов от объекта "Заявитель"

>     models.Person.objects.first().appeals.create(status='worked', 
>                                                  number_of_victims=0, 
>                                                  not_call=False)

---
##### 3. Добавьте к "Обращению" несколько "экстренных служб" двумя способами (add, set)

>     models.Appeal.objects.last().service.add(1, 2)
> 
>     services = models.Service.objects.all()
>     models.Appeal.objects.last().service.set(services)

---
## Запросы в БД

---
##### 1. Получить объект заявителя с идентификатором в базе данных = 1 тремя способами

>-     models.Person.objects.get(id=1)
>-     models.Person.objects.filter(id=1).first()
>-     models.Person.objects.order_by('id').first()

---
##### 2. Получить все обращения заявителя двумя способами

>-     models.Person.objects.first().appeals.all()
>-     models.Appeal.objects.filter(person__id=1)

---
##### 3. Получить первые три экстренные службы

>     models.Service.objects.all()[:3]

---
##### 4. Получить последние пять заявителей

>     models.Person.objects.reverse()[:5]

---
##### 5. Получить самое старое и самое новое обращения двумя способами (latest, earliest, order_by)

>-     models.Appeal.objects.latest('date')
>-     models.Appeal.objects.earliest('date')
>-     models.Appeal.objects.order_by('date').first()
>-     models.Appeal.objects.order_by('-date').first()

---
##### 6. Получить каждое второе обращение

>     models.Appeal.objects.all()[::2]

---
##### 7. Если дважды проитерироваться по полученному QuerySet'y, то сколько будет сделано обращений в БД?

> 2 **

---
##### 8. Вывести общее число обращений

>     models.Appeal.objects.count()

---
##### 9. Получить случайное обращение

>     models.Appeal.objects.order_by('?').first()

---
## Фильтрация

---
##### 1. Получить обращения с заявителем, идентификатор которого равен 1

>     models.Person.objects.get(id=1).appeals.all()
>     models.Appeal.objects.filter(person__id=1)

---
##### 2. Получить всех заявителей определенного пола и без обращений

>     models.Person.objects.filter(gender='Ж', appeals__isnull=True)

---
##### 3. Отсортировать всех заявителей по идентификатору 

>     models.Person.objects.order_by('id')

---
##### 4. Получить всех несовершеннолетних заявителей 

>     import datetime
> 
> 
>     date = datetime.date.today()
>     models.Person.objects.filter(birthdate__gte=datetime.date(date.year-18, 
>                                                               date.month, 
>                                                               date.day))

---
##### 5. Получить всех совершеннолетних заявителей

>     import datetime
> 
> 
>     date = datetime.date.today()
>     models.Person.objects.filter(birthdate__lte=datetime.date(date.year-18, 
>                                                               date.month, 
>                                                               date.day))

---
##### 6. Узнать, есть ли вообще какие-нибудь заявители

>     models.Person.objects.exists()

##### 7. Узнать, есть ли какие-нибудь заявители с похожими именами

>     models.Person.objects.filter(first_name__icontains='иван')

---
##### 8. Получить все обращения, кроме тех у которых не назначены службы

>     models.Appeal.objects.filter(service__isnull=True)

---
##### 9. Среди обращений со службой "03", вывести дату самого первого обращения

>     models.Appeal.objects.filter(service__code='03').earliest('date').date.date()

---
##### 10. Получить все обращения, которые созданы до определенной даты

>     date = 'YYYY-MM-DD' 
>     models.Appeal.objects.filter(date__lt=date)

---
##### 11. Получить всех заявителей без изображения и/или без номера телефона

>     from django.db.models import Q
> 
> 
>     models.Person.objects.filter(Q(photo='') & Q(number=None))
>     models.Person.objects.filter(Q(photo='') | Q(number=None))

---
##### 12. Получить всех заявителей, с определенным кодом оператора (917)

>     models.Person.objects.filter(number__startswith=8917)

---
##### 13. Получить результат объединения, пересечения и разницы предыдущих запросов

>     from django.db.models import Q
>
>
>     a = models.Person.objects.filter(Q(photo='') | Q(number=None))
>     b = models.Person.objects.filter(number__startswith=899)

 Команды SQLite не поддерживает как понял, поэтому придётся соорудировать в ручную
>-     a | b
>-     a & b
>-     a ^ b
>
 А в основном:
>-     a.union(b)
>-     a.intersection(b)
>-     a.difference(b)

---
##### 14. Вывести все обращения, созданные в определенный период

>     models.Appeal.objects.filter(date__date__gt=datetime.date(2022, 12, 6), 
>                                  date__date__lt=datetime.date(2022, 12, 10))

---
##### 15. Получить количество заявителей без номера телефона

>     models.Person.objects.filter(phone_number__isnull=True).count()

---
##### 16. Вывести все уникальные записи модели заявитель

>     models.Person.objects.distinct() SQLite не умеет, прочитать про них

---
##### 17. Получить все обращения, в описании которых есть какое-то ключевое слово в любом регистре
 
>     models.Appeal.objects.filter(appeals__icontains='ог')

---
##### 18. Выбрать всех заявителей, при этом получить только значения поля "номер телефона"

>     models.Person.objects.only('phone_number')

---
##### 19. Выбрать всех заявителей, при этом получить всем поля, кроме состояния здоровья

>     models.Person.objects.defer(health_condition)

---
##### 20. Вывести все службы используя sql запрос

>     for i in models.Service.objects.raw('SELECT * FROM save_service'):
>         print(i)

---
##### 21. Выберите или создайте заявителя с номером "12341234"

>     models.Person.objects.update_or_create(phone_number=123123,
>                                            defaults={
>                                                   'last_name': 'Сорокина', 
>                                                   'first_name': 'Анна', 
>                                                   'patronymic': 'Фанильевна', 
>                                                   'birthdate': '2020-09-09', 
>                                                   'gender': 'Ж', 
>                                                   'phone_number': 12341234
>                                                      })

---
##### 22. Измените номер заявителей с номером "12341234" на любой другой, если заявителя, то запрос должен его создать

>     models.Person.objects.update_or_create(phone_number=12341234, 
>                                            defaults={'phone_number': 88009005060})

---
##### 23. Создайте сразу несколько заявителей

>     models.Person.objects.bulk_create([
>                             models.Person(last_name='Артемов', 
>                                           first_name='Артем',
>                                           patronymic='Артемович', 
>                                           birthdate='2010-10-10', 
>                                           gender='М'),
>                             models.Person(last_name='Артемов', 
>                                           first_name='Артем', 
>                                           patronymic='Артемович', 
>                                           birthdate='2010-10-10', 
>                                           gender='М'),
>                             models.Person(last_name='Артемов', 
>                                           first_name='Артем', 
>                                           patronymic='Артемович', 
>                                           birthdate='2010-10-10', 
>                                           gender='М'),
>                                      ])
---
##### 24. Измените несколько заявителей. Для пола "состояния здоровья" задайте значение "Полностью здоров"

>     models.Person.objects.filter(phone_number=88009003030).update(health_condition='Полностью здоров')

---
##### 25. Выведите имя заявителя у какого-либо обращения. Убедитесь, что было сделано не более одного запроса

>     models.Appeal.objects.select_related('person').first().person.first_name

---
##### 26. Выведите список всех обращений с указанием списка задействованных экстренных служб в следующем формате: "номер обращения: , список кодов служб: ". Убедитесь, что было сделано не более двух запросов в БД

>     appeals = models.Appeal.objects.prefetch_related('service').all()
>     for appeal in appeals:
>         print(f'{appeal.number}: {appeal.service.all().values_list("code", flat=True)}')

---
##### 27. Выведите все значения дат создания происшествий. Поместите даты в список

>     list(models.Appeal.objects.values_list('date__date', flat=True))

---
##### 28. Создайте queryset, который будет всегда пустым

>     models.Person.objects.none()

---
##### 29. Вывести среднее количество пострадавших в происшествиях

>     from django.db.models import Avg
> 
> 
>     models.Appeal.objects.aggregate(Avg('number_of_victims'))

---
##### 30. Вывести общее количество пострадавших в происшествиях

>     from django.db.models import Sum
> 
> 
>     models.Appeal.objects.aggregate(Sum('number_of_victims'))

---
##### 31. Вывести количество вызванных экстренных служб для каждого происшествия

>     from django.db.models import Count
>
> 
>     models.Appeal.objects.annotate(Count('service')).values_list('number', 'service__count')

---
##### 32. Вывести среднее количество вызванных экстренных служб

>     from django.db.models import Count, Avg, IntegerField
>
>
>     models.Appeal.objects.annotate(Count('service')).aggregate(Avg('service__count', output_field=IntegerField()))

---
##### 33. Вывести наибольшее и наименьшее количество пострадавших

>     from django.db.models import Max, Min
> 
> 
>     models.Appeal.objects.aggregate(Max('number_of_victims'))
>
>     models.Appeal.objects.aggregate(Min('number_of_victims'))

---
##### 34. Сформировать запрос к модели заявитель, в котором будет добавлено поле с количеством обращений каждого заявителя

>     from django.db.models import Count
>
> 
>     models.Person.objects.annotate(Count('appeals')).values_list('last_name', 'first_name', 'appeals__count')

---
- ### Дополнительно


---
##### 1. Всем обращениям, у которых назначены службы, присвоить статус "Завершено"

>     from django.db.models import Count
> 
> 
>     models.Appeal.objects.filter(service__isnull=False).update(status='finish')

---
##### 2. Удалить всех заявителей без номера телефона

>     models.Person.objects.filter(phone_number__isnull=True).delete()