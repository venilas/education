from django.contrib import admin
from save import models


class Service(admin.ModelAdmin):
    fields = ('name', 'code', 'number')
    readonly_fields = ('code', )
    list_display = ('id', 'name', 'code', 'number')
    list_display_links = ('id', 'name', 'code', 'number')


admin.site.register(models.Service, Service)


@admin.register(models.Person)
class Person(admin.ModelAdmin):
    list_display = ('id', 'last_name', 'first_name', 'patronymic', 'birthdate', 'phone_number', 'health_condition', 'gender')
    list_display_links = ('id', 'phone_number')
    search_fields = ('last_name', 'first_name', 'patronymic', 'phone_number')
    radio_fields = {'gender': admin.HORIZONTAL}


@admin.register(models.Appeal)
class Appeal(admin.ModelAdmin):
    list_display = ('date', 'number', 'person', 'status', 'number_of_victims', 'not_call')
    list_display_links = ('number',)
    list_filter = ('date',)
    ordering = ('-date',)
