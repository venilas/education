from django import forms
from django.core.exceptions import ValidationError

from .models import *
import datetime


class AddAppealForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        widgets = {
            'service': forms.CheckboxInput(),
        }

    class Meta:
        model = Appeal
        fields = ('person', 'status', 'number_of_victims', 'not_call', 'service')


class AddPersonForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        widgets = {
            'health_condition': forms.Textarea(attrs={'cols': 50, 'rows': 20}),
        }

    class Meta:
        model = Person
        fields = ('last_name', 'first_name', 'patronymic', 'birthdate', 'gender', 'phone_number', 'health_condition')

    def clean_birthdate(self):
        birthdate = self.cleaned_data['birthdate']
        if birthdate > datetime.datetime.today().date():
            raise ValidationError('Введите правильную дату рождения')

        return birthdate

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']
        if len(str(phone_number)) > 11:
            raise ValidationError('Длина номера телефона должна быть 11')


class AddServiceForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = ('name', 'code', 'number')
        help_texts = {
            'code': 'Пример: 101, 102, 103',
        }
