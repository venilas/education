import uuid
from django.db import models


class Service(models.Model):
    """"Класс службы."""
    name = models.CharField('Название службы', max_length=255)
    code = models.CharField('Код службы', max_length=255)
    number = models.PositiveIntegerField('Номер телефона службы')

    class Meta:
        verbose_name = 'Служба'
        verbose_name_plural = 'Службы'
        ordering = ('-code',)

    def __str__(self):
        return self.name


class Person(models.Model):
    """Класс заявителя."""
    last_name = models.CharField('Фамилия', max_length=255)
    first_name = models.CharField('Имя', max_length=255)
    patronymic = models.CharField('Отчество', max_length=255)
    birthdate = models.DateField('Дата рождения')
    phone_number = models.PositiveIntegerField('Номер телефона', blank=True, null=True)
    health_condition = models.TextField(
        'Состояния здоровья',
        blank=True,
        default='практически здоров',
        help_text='аллергоанамнез, хронические заболевание и т.п.'
    )
    GENDER = [
        ('М', 'Мужской'),
        ('Ж', 'Женский'),
    ]
    gender = models.CharField('Пол', max_length=255, choices=GENDER, blank=True)
    photo = models.ImageField('Изображение', blank=True)

    class Meta:
        verbose_name = 'Заявитель'
        verbose_name_plural = 'Заявители'
        ordering = ('last_name', 'first_name', 'patronymic')

    def full_name(self):
        return f'{self.last_name} {self.first_name} {self.patronymic}'

    def __str__(self):
        return self.full_name()


class Appeal(models.Model):
    """Класс обращения."""
    date = models.DateTimeField('Дата', auto_now_add=True)
    number = models.UUIDField(
        'Номер карточки',
        default=uuid.uuid4,
        editable=False,
        unique=True
    )  # что за индексирование, db_index
    person = models.ForeignKey(
        Person,
        verbose_name='Заявитель',
        related_name='appeals',
        on_delete=models.CASCADE
    )
    PRO = 'worked'
    END = 'finish'
    STATUS_CHOICES = [
        (PRO, 'В работе'),
        (END, 'Завершено'),
    ]
    status = models.CharField('Статус', max_length=255, default=PRO, choices=STATUS_CHOICES)
    number_of_victims = models.PositiveIntegerField('Количество пострадавших')
    not_call = models.BooleanField('Не звонить')
    service = models.ManyToManyField(Service, related_name='appeals')

    class Meta:
        verbose_name = 'Обращение'
        verbose_name_plural = 'Обращения'
        ordering = ('date', 'number')

    def __str__(self):
        return f'{self.number}'
