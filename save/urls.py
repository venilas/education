from django.urls import path, register_converter
from save import views

# register_converter(views.PersonDetailConverter, 'num')

app_name = 'save'
urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),
    path('persons/', views.PersonsView.as_view(), name='persons'),
    path('person/<int:pk>', views.PersonDetailView.as_view(), name='person'),
    path('person/', views.person_detail_get, name='person_detail_get'),
    path('person/add/', views.add_person, name='add_person'),
    path('appeals/', views.AppealsView.as_view(), name='appeals'),
    path('appeal/<int:pk>', views.appeal, name='appeal'),
    path('appeal/add/', views.add_appeal, name='add_appeal'),
    path('services/', views.ServicesView.as_view(), name='services'),
    path('service/<int:pk>', views.service, name='service'),
    path('service/add/', views.add_service, name='add_service'),
]
