from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.views.generic import TemplateView, ListView, DetailView

from save.models import *
from save.forms import *
from django.db.models import Avg, Count


class IndexView(TemplateView):
    template_name = 'save/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Главная страница'
        context['count'] = Appeal.objects.count()
        return context


class PersonDetailView(DetailView):
    model = Person
    template_name = 'save/person.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Информация о заявителе'
        return context


def person_detail_get(request):
    number = request.GET.get('number')
    qs = Person.objects.filter(phone_number=number)
    return JsonResponse(qs.values().first())


# class PersonDetailConverter:
#     regex = '[0-9]{11}'
#
#     def to_python(self, value):
#         return int(value)


class PersonsView(TemplateView):
    template_name = 'save/persons.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['persons'] = Person.objects.all()
        return context


def add_person(request):
    if request.method == 'POST':
        form = AddPersonForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('persons')
    else:
        form = AddPersonForm()
    return render(request, 'save/add_person.html', {'form': form, 'title': 'Добавление заявителя'})


class AppealsView(ListView):
    model = Appeal
    template_name = 'save/appeals.html'
    context_object_name = 'appeals'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['average_amount'] = Appeal.objects.annotate(Count('service')).aggregate(Avg('service__count'))['service__count__avg']
        return context


def add_appeal(request):
    if request.method == 'POST':
        form = AddAppealForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('appeals')
    else:
        form = AddAppealForm()
    return render(request, 'save/add_appeal.html', {'form': form, 'title': 'Добавить обращение'})


def appeal(request, pk):
    qs = Appeal.objects.filter(id=pk)
    return JsonResponse(qs.values().first())


class ServicesView(TemplateView):
    template_name = 'save/services.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['services'] = Service.objects.all()
        return context


def service(request, pk):
    qs = Service.objects.filter(id=pk)
    return JsonResponse(qs.values().first())


def add_service(request):
    if request.method == 'POST':
        form = AddServiceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('services')
    else:
        form = AddServiceForm()
    return render(request, 'save/add_service.html', {'form': form, 'title': 'Добавление службы'})
